package cpsc2150.banking.controllers;

import cpsc2150.banking.models.*;
import cpsc2150.banking.views.*;

/**
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class MortgageGUIController implements IMortgageController {
    private IMortgageView view;

    public MortgageGUIController(IMortgageView v) {
        view = v;
    }

    public void submitApplication() {
        // Data needed to initialize customer
        String name;
        int creditScore;
        double yearlyIncome;
        double monthlyDebt;

        // Data needed to initialize mortgage
        double houseCost;
        double downPayment;
        int years;

        // Get name
        name = view.getName();
        if (name.length() <= 0) {
            view.printToUser("Please enter a name");
            return;
        }

        // Get yearly income
        yearlyIncome = view.getYearlyIncome();
        if (yearlyIncome < 0) {
            view.printToUser("Yearly income must be greater than or equal to 0");
            return;
        }

        // Get monthly debt
        monthlyDebt = view.getMonthlyDebt();
        if (monthlyDebt < 0) {
            view.printToUser("Monthly debt must be greater than or equal to 0");
            return;
        }

        // Get credit score
        creditScore = view.getCreditScore();
        if (creditScore < 0 || creditScore > ICustomer.MAX_CREDIT_SCORE) {
            view.printToUser(String.format("Please enter a credit score between %d and %d", 0, ICustomer.MAX_CREDIT_SCORE));
            return;
        }

        // Get house cost
        houseCost = view.getHouseCost();
        if (houseCost < 0) {
            view.printToUser("House cost must be greater than or equal to 0");
            return;
        }

        // Get down payment
        downPayment = view.getDownPayment();
        if (downPayment < 0) {
            view.printToUser("Down payment must be greater than or equal to 0");
            return;
        }

        // Get number of years
        years = view.getYears();
        if (years < IMortgage.MIN_YEARS || years > IMortgage.MAX_YEARS) {
            view.printToUser(String.format("Years must be between %d and %d", IMortgage.MIN_YEARS, IMortgage.MAX_YEARS));
            return;
        }

        // All info given was correct

        // Instantiate customer with the info given
        ICustomer customer = new Customer(monthlyDebt, yearlyIncome, creditScore, name);

        // Instantiate mortgage
        IMortgage mortgage = new Mortgage(houseCost, downPayment, years, customer);

        // Remove any error message
        view.printToUser("");

        // Show mortgage to user
        view.displayApproved(mortgage.loanApproved());
        view.displayRate(mortgage.getRate());
        view.displayPayment(mortgage.getPayment());
    }
}
