package cpsc2150.banking.controllers;
import cpsc2150.banking.models.*;
import cpsc2150.banking.views.*;

/**
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class MortgageController implements IMortgageController {
    private IMortgageView view;

    public MortgageController(IMortgageView v) {
        view = v;
    }

    public void submitApplication() {
        // Main loop, ask multiple customers about multiple mortgages
        do {

            // What we need to get:
            String name;
            int creditScore;
            double yearlyIncome;
            double monthlyDebt;

            // Get name
            while (true) {
                name = view.getName();
                if (name.length() > 0) break;
                view.printToUser("Please enter a name");
            }

            // Get yearly income
            while (true) {
                yearlyIncome = view.getYearlyIncome();
                if (yearlyIncome >= 0) break;
                view.printToUser("Yearly income must be greater than or equal to 0");
            }

            // Get monthly debt
            while (true) {
                monthlyDebt = view.getMonthlyDebt();
                if (monthlyDebt >= 0) break;
                view.printToUser("Monthly debt must be greater than or equal to 0");
            }

            // Get credit score
            while (true) {
                creditScore = view.getCreditScore();
                if (creditScore >= 0 && creditScore <= ICustomer.MAX_CREDIT_SCORE)
                    break;
                view.printToUser(String.format("Please enter a credit score between %d and %d", 0, ICustomer.MAX_CREDIT_SCORE));
            }

            // Instantiate customer with the info given
            ICustomer customer = new Customer(monthlyDebt, yearlyIncome, creditScore, name);

            // Ask a customer about multiple mortgages
            do {
                // Data needed to initialize mortgage
                double houseCost;
                double downPayment;
                int years;

                // Get house cost
                while (true) {
                    houseCost = view.getHouseCost();
                    if (houseCost >= 0) break;
                    view.printToUser("House cost must be greater than or equal to 0");
                }

                // Get down payment
                while (true) {
                    downPayment = view.getDownPayment();
                    if (downPayment >= 0) break;
                    view.printToUser("Down payment must be greater than or equal to 0");
                }

                // Get number of years
                while (true) {
                    years = view.getYears();
                    if (years >= IMortgage.MIN_YEARS && years <= IMortgage.MAX_YEARS) break;
                    view.printToUser(String.format("Years must be between %d and %d", IMortgage.MIN_YEARS, IMortgage.MAX_YEARS));
                }

                // Instantiate mortgage
                IMortgage mortgage = new Mortgage(houseCost, downPayment, years, customer);

                // Show mortgage to user
                view.printToUser(customer.toString() + mortgage.toString());

            // Ask user if they want to enter another mortgage
            } while (view.getAnotherMortgage());

        // Ask user if they want to enter another customer
        } while (view.getAnotherCustomer());
    }
}