package cpsc2150.banking;

import cpsc2150.banking.views.*;
import cpsc2150.banking.controllers.*;

/**
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class MortgageApp {

    public static void main(String [] args) {
        IMortgageView view = new MortgageView();
        IMortgageController controller = new MortgageController(view);
        view.setController(controller);
        controller.submitApplication();
    }
}