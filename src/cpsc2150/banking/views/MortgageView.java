package cpsc2150.banking.views;

import cpsc2150.banking.controllers.IMortgageController;
import java.util.Scanner;

/**
 * @author Jacob Collins
 * @author Nicholas Childers
 */
public class MortgageView implements IMortgageView {
    private final Scanner stdin = new Scanner(System.in);
    private IMortgageController controller;

    public void setController(IMortgageController c) {
        controller = c;
    }

    public double getHouseCost() {
        System.out.println("How much does the house cost?");
        return stdin.nextDouble();
    }

    public double getDownPayment() {
        System.out.println("How much is the down payment?");
        return stdin.nextDouble();
    }

    public int getYears() {
        System.out.println("How many years?");
        return stdin.nextInt();
    }

    public double getMonthlyDebt() {
        System.out.println("How much are your monthly debt payments?");
        return stdin.nextDouble();
    }

    public double getYearlyIncome() {
        System.out.println("How much is your yearly income?");
        return stdin.nextDouble();
    }

    public int getCreditScore() {
        System.out.println("What is your credit score?");
        return stdin.nextInt();
    }

    public String getName() {
        System.out.println("What's your name?");
        return stdin.nextLine();
    }

    public void printToUser(String s) {
        System.out.println(s);
    }

    public void displayPayment(double p) {
        System.out.println("The monthly mortgage payment amount:");
        System.out.println(p);
    }

    public void displayRate(double r) {
        System.out.println("The interest rate:");
        System.out.println(r);
    }

    public void displayApproved(boolean a) {
        if (a)
            System.out.println("The loan was approved");
        else
            System.out.println("The loan was not approved");
    }

    public boolean getAnotherMortgage() {
        System.out.println("Would you like to enter another mortgage's information? (Y/N)");
        String line;
        // Make sure the line we get is not empty
        do line = stdin.nextLine(); while (line.length() <= 0);
        char c = Character.toLowerCase(line.charAt(0));
        return c == 'y';
    }

    public boolean getAnotherCustomer() {
        System.out.println("Would you like to enter another user's information? (Y/N)");
        String line;
        // Make sure the line we get is not empty
        do line = stdin.nextLine(); while (line.length() <= 0);
        char c = Character.toLowerCase(line.charAt(0));
        return c == 'y';
    }
}
