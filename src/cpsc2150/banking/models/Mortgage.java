package cpsc2150.banking.models;

/**
 * Non-abstract implementation of the IMortgage interface
 *
 * @author Jacob Collins
 * @author Nicholas Childers
 *
 * Correspondences:
 *     self.Payment = payment
 *     self.Rate = apr / NUM_MONTHS
 *     self.Customer = customer
 *     self.DebtToIncomeRatio = debtToIncomeRatio
 *     self.Principal = principal
 *     self.NumberOfPayments = years * NUM_MONTHS
 *     self.PercentDown  = percentDown
 *
 * @invariant  payment = ((apr / NUM_MONTHS) * principal) / (1-(1+(apr / NUM_MONTHS))^ -(years * 12)) AND
 *             0 <= (apr / NUM_MONTHS) <= 1 AND
 *             0 < debtToIncomeRatio AND
 *             MIN_YEARS <= years <= MAX_YEARS AND
 *             0 < principal AND
 *             0 <= percentDown < 1 AND
 *             customer is a valid ICustomer
 */
public class Mortgage extends AbsMortgage implements IMortgage {

    public static final int NUM_MONTHS = 12;
    private double payment;
    private double principal;
    private double apr = BASERATE;
    private ICustomer customer;
    private double debtToIncomeRatio;
    private int years;
    private double percentDown;

    /**
     * @pre aCost >= 0 AND
            aDownPayment >= 0 AND
            MIN_YEARS <= aNumYears <= MAX_YEARS AND
            customer is a valid ICustomer
       @post Rate is calculated based on the BASERATE, the years for the loan, and the PercentDown AND
             Payment is calculated
     * @param aCost the cost of the house
     * @param aDownPayment the down payment on the house
     * @param aNumYears the term of the mortgage in years
     * @param aCustomer the customer applying for the mortgage
     */
    public Mortgage(double aCost, double aDownPayment, int aNumYears, ICustomer aCustomer) {
        years = aNumYears;
        principal = aCost - aDownPayment;
        customer = aCustomer;
        percentDown = aDownPayment / aCost;

        if (aNumYears < MAX_YEARS) apr += GOODRATEADD;
            else apr += NORMALRATEADD;

        if (aDownPayment / aCost < PREFERRED_PERCENT_DOWN) apr += GOODRATEADD;

        if      (aCustomer.getCreditScore() < BADCREDIT  ) apr += VERYBADRATEADD;
        else if (aCustomer.getCreditScore() < FAIRCREDIT ) apr += BADRATEADD;
        else if (aCustomer.getCreditScore() < GOODCREDIT ) apr += NORMALRATEADD;
        else if (aCustomer.getCreditScore() < GREATCREDIT) apr += GOODRATEADD;

        payment = ((apr / NUM_MONTHS) * principal) / (1 - Math.pow((1 + apr / NUM_MONTHS), -(years * NUM_MONTHS)));
        debtToIncomeRatio = (payment + customer.getMonthlyDebtPayments()) / (customer.getIncome() / NUM_MONTHS);
    }

    @Override
    public boolean loanApproved() {
        return ! (  apr >= RATETOOHIGH
                 || percentDown < MIN_PERCENT_DOWN
                 || debtToIncomeRatio > DTOITOOHIGH
                 );
    }

    @Override
    public double getPayment() {
        return payment;
    }

    @Override
    public double getRate() {
        return apr;
    }

    @Override
    public double getPrincipal() {
        return principal;
    }

    @Override
    public int getYears() {
        return years;
    }
}
